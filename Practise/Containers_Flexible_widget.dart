import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});
  final cont11 = Flexible(
      child: Container(
          margin: const EdgeInsets.fromLTRB(10, 10, 5, 5),
          padding: const EdgeInsets.all(10),
          color: Colors.blueAccent,
          child: const Center(
              child: Text("First solve the problem, then write the code!",
                  style: TextStyle(
                      fontFamily: "Fira code",
                      fontSize: 20,
                      fontWeight: FontWeight.bold)))));

  final cont12 = Flexible(
      child: Container(
          margin: const EdgeInsets.fromLTRB(5, 10, 10, 5),
          padding: const EdgeInsets.all(10),
          color: Colors.pink,
          child: const Center(
              child: Text("Programmer never dies, just goes offline!",
                  style: TextStyle(
                      fontFamily: "Fira code",
                      fontSize: 20,
                      fontWeight: FontWeight.bold)))));
  final cont21 = Flexible(
      child: Container(
          margin: const EdgeInsets.fromLTRB(10, 5, 5, 10),
          padding: const EdgeInsets.all(10),
          color: Colors.lightGreenAccent,
          child: const Center(
              child: Text("Code Never lies, Comments sometimes do!",
                  style: TextStyle(
                      fontFamily: "Fira code",
                      fontSize: 20,
                      fontWeight: FontWeight.bold)))));
  final cont22 = Flexible(
      child: Container(
          margin: const EdgeInsets.fromLTRB(5, 5, 10, 10),
          padding: const EdgeInsets.all(10),
          color: Colors.blueGrey,
          child: const Center(
              child: Text("Talk is cheap show me the code!",
                  style: TextStyle(
                      fontFamily: "Fira code",
                      fontSize: 20,
                      fontWeight: FontWeight.bold)))));

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My App',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: const Text("Home"),
            centerTitle: true,
            leading: const Icon(Icons.search),
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.person),
                tooltip: 'Profile',
                onPressed: () {},
              )
            ],
          ),
          body: Column(
            children: [
              Expanded(child: Row(children: [cont11, cont12])),
              Expanded(child: Row(children: [cont21, cont22]))
            ],
          )),
    );
  }
}
