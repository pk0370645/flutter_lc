import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "My App",
        theme: ThemeData(
          primarySwatch: Colors.amber,
        ),
        home: Scaffold(
            appBar: AppBar(title: const Text("Home")),
            body:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Flexible(
                  flex: 1,
                  child: Row(children: [
                    Flexible(
                        child: Container(
                            margin: const EdgeInsets.fromLTRB(10, 10, 5, 5),
                            padding: const EdgeInsets.all(10),
                            alignment: Alignment.center,
                            color: Colors.purpleAccent,
                            child: const Text("Programmer"))),
                    Flexible(
                        child: Container(
                            margin: const EdgeInsets.fromLTRB(5, 10, 10, 5),
                            padding: const EdgeInsets.all(10),
                            alignment: Alignment.center,
                            color: Colors.limeAccent,
                            child: const Text("Programmer"))),
                  ])),
              Flexible(
                  flex: 3,
                  child: Row(
                    children: [
                      Flexible(
                          child: Container(
                              margin: const EdgeInsets.fromLTRB(10, 5, 5, 10),
                              padding: const EdgeInsets.all(10),
                              alignment: Alignment.center,
                              color: Colors.blueAccent,
                              child: const Text("Programmer"))),
                      Flexible(
                          child: Container(
                              margin: const EdgeInsets.fromLTRB(5, 5, 10, 10),
                              padding: const EdgeInsets.all(10),
                              alignment: Alignment.center,
                              color: Colors.blueAccent,
                              child: const Text("Programmer"))),
                    ],
                  ))
            ])));
  }
}
