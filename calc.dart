import 'dart:io';

int sum(num1, num2){
    return num1+num2;
}


int minus(num1, num2){
    return num1-num2;
}

int product(num1, num2){
    return num1*num2;
}

int division(num1, num2){
    return num1/num2;
}


void main(){
    int? num1, num2;
    String opt;

    stdout.write("Enter first number:");
    num1 = int.parse(stdin.readLineSync()!);

    stdout.write("Enter second number:");
    num2 = int.parse(stdin.readLineSync()!);
    
    stdout.write("Enter the operator:");
    opt = stdin.readLineSync()!;

    if (opt == '+'){
        print("$num1 + $num2 = ${sum(num1, num2)}.");
    }
    else if (opt == '-'){
        print("$num1 - $num2 = ${minus(num1, num2)}.");
    }
    else if (opt == '*'){
        print("$num1 * $num2 = ${product(num1, num2)}.");
    }
    else if (opt == '/'){
        print("$num1 / $num2 = ${division(num1, num2)}.");
    }
    else{
        print("\n\nInvalid Operator!\nTry Again!!!");
    }

}