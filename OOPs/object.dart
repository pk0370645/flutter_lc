class Bicycle {
    String? color;
    int? size, currentSpeed;

    void changeGear(int newValue) {
        currentSpeed = newValue;
    }

    void display(){
        print("Color: $color");
        print("Size: $size");
        print("Current Speed: $currentSpeed");
    }
}


class Animal {
    String? name;
    int? numberOfLegs, lifeSpan;

    void display() {
        print("Animal name: $name");
        print("Number of legs: $numberOfLegs");
        print("Life Span: $lifeSpan");
    }
    
}


class Car {
    String? name, color;
    int? numberOfSeats;

    void start() {
        print("$name Car has Started.");
    }
}

class Camera {
    String? name, color;
    double? megaPixel;

    void display() {
        print("Camera name: $name");
        print("Color: $color");
        print("MegaPixels: $megaPixel");
    }
}


void cycle() {
    // Here bicycle is an object of class Bicycle.
    Bicycle bicycle = Bicycle();
    bicycle.color = "Purple";
    bicycle.size = 26;
    bicycle.currentSpeed = 5;
    bicycle.display();
    print("\n");
    bicycle.changeGear(10);
    bicycle.display();
}

void animal() {
    Animal animal = Animal();
    animal.name = "Lion";
    animal.numberOfLegs = 4;
    animal.lifeSpan = 10;
    animal.display();
}

void car() {
    Car car = Car();
    car.name = "BMW";
    car.color = "Red";
    car.numberOfSeats = 4;
    car.start();

    Car car2 = Car();
    car2.name = "Audi R8";
    car2.color = "Black";
    car2.numberOfSeats = 4;
    car2.start();
}


void camera() {
    Camera camera1 = Camera();
    Camera camera2 = Camera();
    
    camera1.name = "Sony A1";
    camera1.color = "Black";
    camera1.megaPixel = 26.5;

    camera2.name = "Canon RS1";
    camera2.color = "Black";
    camera2.megaPixel = 21.5;

    camera1.display();
    print("\n");
    camera2.display();
}


void main(){
    // cycle();
    // animal();
    // car();
    camera();
}