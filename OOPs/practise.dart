class Book {
    String? name, author, prize;

    void display(){
        print("The name of the book is $name.");
        print("The name of the author of the book is $author.");
        print("The prize of the book is $prize.");
    }
}

class Camera {
    String? name, color, megapixel;

    void display(){
        print("The name of the camera is $name.");
        print("The color of the camera is $color.");
        print("The megapixel count of the camera is $megapixel.");
    }
}

void main(){
    Camera sony = Camera();
    sony.name = "Sony A1";
    sony.color = "Black";
    sony.megapixel = "26.5M";

    Camera canon = Camera();
    canon.name = "Canon R1";
    canon.color = "Grey";
    canon.megapixel = "21.5M";

    sony.display();
    print("\n");
    canon.display();
}