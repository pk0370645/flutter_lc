class Animal {
    String? name;
    int? numberOfLegs, lifeSpan;

    void display(){
        print("Animal name: $name.");
        print("Number of Lengs: $numberOfLegs.");
        print("Life Span: $lifeSpan.");
    }
}


class Person {
    String? name, phone;
    bool? isMarried;
    int? age;

    void displayInfo() {
        print("Person name: $name.");
        print("Phone number: $phone.");
        print("Married: $isMarried.");
        print("Age: $age.");
    }
}


class Area {
    double? length, breadth;

    double calculateArea() {
        return length!*breadth!;
    }
}


class Book {
    String? name, author;
    int? prize;

    void displayInfo() {
        print("Book name: $name.");
        print("author: $author.");
        print("prize: $prize.");
    }
}
void main(){
    
}