class Student {
    String? name;
    int? age, rollNumber;

    // Constructor
    Student(String name, int age, int rollNumber) {
        this.name = name;
        this.age = age;
        this.rollNumber = rollNumber;
    }
}


class Teacher {
    String? name, subject;
    int? age;
    double? salary;

    // Constructor
    Teacher(String name, int age, String subject, double salary){
        this.name = name;
        this.age = age;
        this.subject = subject;
        this.salary = salary;
    }

    // Method
    void display() {
        print("Name: ${this.name}");
        print("Age: ${this.age}");
        print("Subject: ${this.subject}");
        print("Salary: ${this.salary}");
    }

}


class Patient {
    String? name, disease;
    int? age;

    Patient (this.name, this.age, this.disease);

    void display() {
        print("Name: $name");
        print("Age: $age");
        print("Disease: $disease");
    }
}


void student() {
    Student student = Student("Prakash Kushwaha", 19, 27513);
    print("Name: ${student.name}");
    print("Age: ${student.age}");
    print("Roll number: ${student.rollNumber}");
}


void teacher() {
    Teacher teacher1 = Teacher("John", 30, "Maths", 50000.0);
    teacher1.display();
    print("\n");
    Teacher teacher2 = Teacher("Smith", 35, "Science", 60000.0);
    teacher2.display();
}


void patient(){
    Patient patient1 = Patient("John", 45, "Heart");
    patient1.display();
}


void main(){
    // student();
    // teacher();
    patient();
}