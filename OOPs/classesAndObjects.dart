import 'dart:io';


class Rectangle {
    double? length, breadth;

    double area() {
        return length! * breadth!;
    }
}


class SimpleInterest {
    double? principal, rate, time;

    double interest() {
        return (principal! * rate! * time!)/100;
    }
}


class Home {
    String? name, address;
    int? numberOfRooms;

    void display() {
        print("House name: $name");
        print("Address: $address");
        print("Number of Rooms: $numberOfRooms");
    }
}


void si(){
    SimpleInterest simpleInterest = SimpleInterest();

    stdout.write("Enter The Principal Amount:");
    double? principal = double.parse(stdin.readLineSync()!);

    stdout.write("Enter The Rate:");
    double? rate = double.parse(stdin.readLineSync()!);

    stdout.write("Enter The Time:");
    double? time = double.parse(stdin.readLineSync()!);

    simpleInterest.principal=principal;
    simpleInterest.rate=rate;
    simpleInterest.time=time;

    print("Simple Interest is ${simpleInterest.interest()}.");

}


void rectangle() {
    double? length, breadth;
    stdout.write("Enter length of the rectangle:");
    length = double.parse(stdin.readLineSync()!);

    stdout.write("Enter the breadth of the rectangle:");
    breadth = double.parse(stdin.readLineSync()!);

    Rectangle rectangle = Rectangle();
    rectangle.length = length;
    rectangle.breadth = breadth;
    double? area = rectangle.area();
    print("The area of the rectangle is $area.");
}


void home() {
    Home home1 = Home();
    home1.name = "My Sweet Home";
    home1.address = "H.no-76, D-Block, Jai Vihar, Najafgarh N.D.-43";
    home1.numberOfRooms = 10;

    home1.display();
}


void main() {
    // rectangle();
    // si();
    home();
}