import 'dart:io';

void main(){
    stdout.write("Enter a number:");
    String? num = stdin.readLineSync()!;
    String rev_num='';
    for (int i=(num.length)-1;i>=0;i--){
        rev_num=rev_num+num[i];
    }
    print("The reverse of $num is $rev_num.");
}
