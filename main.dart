import 'package:flutter/material.dart';


void main(){
    runApp(const MyApp());
}

class MyApp extends StatelessWidget {
    const MyApp({super.key});
    @override
    Widget build(BuildContext context){
        return MaterialApp(
            title: "My First App",
            theme: ThemeData(primaryColor: Colors.blue,),
            home: Scaffold(
                appBar: AppBar(title: const Text("Home Page")),
                body: const Center(child: Text("Hello World")),
            ),
        );
    }
}