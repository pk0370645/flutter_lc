import 'dart:io';

void sum() {
  print("Enter first number:");
  int? num1 = int.parse(stdin.readLineSync()!);

  print("Enter second number:");
  int? num2 = int.parse(stdin.readLineSync()!);
  print("The sum of $num1 and $num2 is ${num1 + num2}.");
}

void main() {
  sum();
}

// import 'dart:io';

// void main() {
//   print("Enter number:");
//   int? number = int.parse(stdin.readLineSync()!);
//   print("The entered number is ${number}");
// }