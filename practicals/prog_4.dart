import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My App",
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Home"),
        ),
        body: Container(
          color: Colors.blue,
          width: 160,
          height: 120,
          margin: const EdgeInsets.all(20),
          child: const Text("Lorem Ispum",
              style: TextStyle(
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w800,
                  color: Colors.black,
                  letterSpacing: 0.5,
                  fontSize: 18,
                  height: 2)),
        ),
      ),
    );
  }
}
