import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final image1 = Row(
      mainAxisSize: MainAxisSize.min,
      children: [Image.asset('images/image1.jpg')]);

  final image2 = Row(
      mainAxisSize: MainAxisSize.min,
      children: [Image.asset('images/image2.jpg')]);

  final image3 = Row(
    mainAxisSize: MainAxisSize.min,
    children: [Image.asset('images/image3.jpg')],
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My App",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          appBar: AppBar(title: const Text("Home")),
          body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(child: image1),
                Expanded(child: image2),
                Expanded(child: image3)
              ])),
    );
  }
}
