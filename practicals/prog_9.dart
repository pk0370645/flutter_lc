import 'package:flutter/material.dart';

void main() {
  runApp(const ProfileApp());
}

class ProfileApp extends StatelessWidget {
  const ProfileApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: const MyCustomForm(),
    );
  }
}

// Define a custom Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({super.key});

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

enum Question1 { option1, option2, option3, option4 }

enum Question2 { option1, option2, option3, option4 }

enum Question3 { option1, option2, option3, option4 }

enum Question4 { option1, option2, option3, option4 }

enum Question5 { option1, option2, option3, option4 }

// Define a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>`,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  Question1? _question1;
  Question2? _question2;
  Question3? _question3;
  Question4? _question4;
  Question5? _question5;

  int correctCount = 0;
  final descTextStyle = const TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w800,
    fontFamily: 'Roboto',
    letterSpacing: 0.5,
    fontSize: 23,
    height: 2,
  );
  final headingTextStyle = const TextStyle(
    color: Colors.blue,
    fontWeight: FontWeight.w800,
    fontFamily: 'Roboto',
    letterSpacing: 0.5,
    fontSize: 32,
    height: 2,
  );

  var heading,
      question1Title,
      question2Title,
      question3Title,
      question4Title,
      question5Title;

  @override
  void initState() {
    super.initState();

    heading = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [Text(
      'Flutter MCQ Questions',
      style: headingTextStyle,
    )],);
    question1Title = Text(
      'Q.1 - Which Language is used by Flutter?',
      style: descTextStyle,
    );
    question2Title = Text(
      'Q.2 - Who developed the Flutter Framework and continues to maintain it today?',
      style: descTextStyle,
    );
    question3Title = Text(
      'Q.3 - What is Flutter?',
      style: descTextStyle,
    );
    question4Title = Text(
      'Q.4 - Access to a cloud database through Flutter is available through which service?',
      style: descTextStyle,
    );
    question5Title = Text(
      'Q.5 - Which file is need to be edit for changing requirements of flutter app?',
      style: descTextStyle,
    );
    //
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Scaffold(
        appBar: AppBar(
          title: const Text("Flutter Quiz App"),
        ),
        // backgroundColor: Colors.grey,
        body: Container(
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              border: Border.all(width: 2),
              // color: const Color.fromARGB(100, 110, 100, 100),
            ),
            child: Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  // Add TextFormFields and ElevatedButton here.
                  heading,
                  question1Title,
                  ListTile(
                    title: const Text('Dart'),
                    leading: Radio<Question1>(
                      toggleable: true,
                      value: Question1.option1,
                      groupValue: _question1,
                      onChanged: (Question1? value) {
                        setState(() {
                          _question1 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('C++'),
                    leading: Radio<Question1>(
                      toggleable: true,
                      value: Question1.option2,
                      groupValue: _question1,
                      onChanged: (Question1? value) {
                        setState(() {
                          _question1 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('Java'),
                    leading: Radio<Question1>(
                      toggleable: true,
                      value: Question1.option3,
                      groupValue: _question1,
                      onChanged: (Question1? value) {
                        setState(() {
                          _question1 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('C#'),
                    leading: Radio<Question1>(
                      toggleable: true,
                      value: Question1.option4,
                      groupValue: _question1,
                      onChanged: (Question1? value) {
                        setState(() {
                          _question1 = value;
                        });
                      },
                    ),
                  ),
                  question2Title,
                  ListTile(
                    title: const Text('Microsoft'),
                    leading: Radio<Question2>(
                      toggleable: true,
                      value: Question2.option1,
                      groupValue: _question2,
                      onChanged: (Question2? value) {
                        setState(() {
                          _question2 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('Facebook'),
                    leading: Radio<Question2>(
                      toggleable: true,
                      value: Question2.option2,
                      groupValue: _question2,
                      onChanged: (Question2? value) {
                        setState(() {
                          _question2 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('Google'),
                    leading: Radio<Question2>(
                      toggleable: true,
                      value: Question2.option3,
                      groupValue: _question2,
                      onChanged: (Question2? value) {
                        setState(() {
                          _question2 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('Oracle'),
                    leading: Radio<Question2>(
                      toggleable: true,
                      value: Question2.option4,
                      groupValue: _question2,
                      onChanged: (Question2? value) {
                        setState(() {
                          _question2 = value;
                        });
                      },
                    ),
                  ),
                  question3Title,
                  ListTile(
                    title: const Text(
                        'Flutter is an open-source backend development framework'),
                    leading: Radio<Question3>(
                      toggleable: true,
                      value: Question3.option1,
                      groupValue: _question3,
                      onChanged: (Question3? value) {
                        setState(() {
                          _question3 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('Flutter is an open-source UI toolkit'),
                    leading: Radio<Question3>(
                      toggleable: true,
                      value: Question3.option2,
                      groupValue: _question3,
                      onChanged: (Question3? value) {
                        setState(() {
                          _question3 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text(
                        'Flutter is an open-source programming language for cross-platform applications'),
                    leading: Radio<Question3>(
                      toggleable: true,
                      value: Question3.option3,
                      groupValue: _question3,
                      onChanged: (Question3? value) {
                        setState(() {
                          _question3 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('Flutters is a DBMS toolkit'),
                    leading: Radio<Question3>(
                      toggleable: true,
                      value: Question3.option4,
                      groupValue: _question3,
                      onChanged: (Question3? value) {
                        setState(() {
                          _question3 = value;
                        });
                      },
                    ),
                  ),
                  question4Title,
                  ListTile(
                    title: const Text('SQLite'),
                    leading: Radio<Question4>(
                      toggleable: true,
                      value: Question4.option1,
                      groupValue: _question4,
                      onChanged: (Question4? value) {
                        setState(() {
                          _question4 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('Firebase Database'),
                    leading: Radio<Question4>(
                      toggleable: true,
                      value: Question4.option2,
                      groupValue: _question4,
                      onChanged: (Question4? value) {
                        setState(() {
                          _question4 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('NOSQL'),
                    leading: Radio<Question4>(
                      toggleable: true,
                      value: Question4.option3,
                      groupValue: _question4,
                      onChanged: (Question4? value) {
                        setState(() {
                          _question4 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('MYSQL'),
                    leading: Radio<Question4>(
                      toggleable: true,
                      value: Question4.option4,
                      groupValue: _question4,
                      onChanged: (Question4? value) {
                        setState(() {
                          _question4 = value;
                        });
                      },
                    ),
                  ),
                  question5Title,
                  ListTile(
                    title: const Text('Requirements.txt'),
                    leading: Radio<Question5>(
                      toggleable: true,
                      value: Question5.option1,
                      groupValue: _question5,
                      onChanged: (Question5? value) {
                        setState(() {
                          _question5 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('pubsec.lock'),
                    leading: Radio<Question5>(
                      toggleable: true,
                      value: Question5.option2,
                      groupValue: _question5,
                      onChanged: (Question5? value) {
                        setState(() {
                          _question5 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('pubspec.yaml'),
                    leading: Radio<Question5>(
                      toggleable: true,
                      value: Question5.option3,
                      groupValue: _question5,
                      onChanged: (Question5? value) {
                        setState(() {
                          _question5 = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('.metadata'),
                    leading: Radio<Question5>(
                      toggleable: true,
                      value: Question5.option4,
                      groupValue: _question5,
                      onChanged: (Question5? value) {
                        setState(() {
                          _question5 = value;
                        });
                      },
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            onPressed: () {
                              correctCount = 0;
                              if (_question1 == Question1.option1) {
                                correctCount += 1;
                              }
                              if (_question2 == Question2.option3) {
                                correctCount += 1;
                              }
                              if (_question3 == Question3.option3) {
                                correctCount += 1;
                              }
                              if (_question4 == Question4.option2) {
                                correctCount += 1;
                              }
                              if (_question5 == Question5.option3) {
                                correctCount += 1;
                              }
                              // Validate returns true if the form is valid, or false otherwise.
                              if (_formKey.currentState!.validate()) {
                                showDialog<String>(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      AlertDialog(
                                    title: Text('You Scored : $correctCount'),
                                    content: const Text('Your Quiz Score'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'Cancel'),
                                        child: const Text('Cancel'),
                                      ),
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'OK'),
                                        child: const Text('OK'),
                                      ),
                                    ],
                                  ),
                                );
                              }
                            },
                            child: const Text('Submit'),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  _question1 = null;
                                  _question2 = null;
                                  _question3 = null;
                                  _question4 = null;
                                  _question5 = null;
                                });
                              },
                              child: const Text('Clear')),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            )));
  }
}
