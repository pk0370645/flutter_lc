import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  final stars = Icons.star;

  Widget foodItems() {
    return ListView(
      children: [
        _tile('French Fries', 'Veg', Icons.food_bank),
        _tile('Burger', 'Veg', Icons.food_bank),
        _tile('Pizza', 'Veg/Non-Veg', Icons.food_bank),
        _tile('Samosa', 'Veg', Icons.food_bank),
        _tile('Momos', 'Veg', Icons.food_bank),
        _tile('Chicken Wings', 'Veg', Icons.food_bank),
        const Divider(),
        _tile('Sahi Paneer', 'Veg', Icons.restaurant),
        _tile('Dal Makhani', 'Veg', Icons.restaurant),
        _tile('Tamatar Sev', 'Veg', Icons.restaurant),
        _tile('Palak Paneer', 'Veg', Icons.restaurant),
        _tile('Chicken Leg Piece', 'Non-Veg', Icons.restaurant),
        _tile('Chicken Changezi', 'Non-Veg', Icons.restaurant),
        const Divider(),
        _tile('Fried Rice', 'Veg', Icons.restaurant),
        _tile('Chapati', 'Veg', Icons.restaurant),
        _tile('Puri', 'Veg', Icons.restaurant),
        _tile('Butter Naan', 'Veg', Icons.restaurant),
      ],
    );
  }

  ListTile _tile(String title, String subtitle, IconData icon) {
    return ListTile(
      title: Text(title,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 20,
          )),
      subtitle: Text(subtitle),
      leading: Icon(
        icon,
        color: Colors.blue[500],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My App",
      theme: ThemeData(
        primarySwatch: Colors.yellow,
        brightness: Brightness.dark,
      ),
      home: Scaffold(
        appBar: AppBar(title: const Text("Home")),
        body: foodItems(),
        // Column(children: [foodPrice()]),
      ),
    );
  }
}
