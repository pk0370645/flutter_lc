import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final column1 =
      Column(mainAxisAlignment: MainAxisAlignment.center, children: [
    Expanded(
        child: Container(
            margin: const EdgeInsets.all(20),
            child: Image.asset('images/profile.jpg'))),
  ]);

  final column2 = Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        margin: const EdgeInsets.all(20),
        child: const Text("About",
            style: TextStyle(
              fontSize: 28,
              fontWeight: FontWeight.w900,
              color: Colors.black,
            )),
      ),
      const Text("Name: John Doe",
          style: TextStyle(
            fontSize: 18,
            color: Colors.black,
          )),
      const Text("Email: john.doe@gmail.com",
          style: TextStyle(
            fontSize: 18,
            color: Colors.black,
          )),
      const Text("Address: Lane - 43, Wilson Street, LA",
          style: TextStyle(
            fontSize: 18,
            color: Colors.black,
          ))
    ],
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'My App',
        theme: ThemeData(
          primarySwatch: Colors.purple,
          // brightness: Brightness.dark,
        ),
        home: Scaffold(
          appBar: AppBar(title: const Text("Profile")),
          // backgroundColor: Colors.grey,
          body: Center(
            child: Row(children: [
              Expanded(
                child: column1,
              ),
              Expanded(
                child: column2,
              )
            ]),
          ),
        ));
  }
}
