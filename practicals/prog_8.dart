import 'dart:math';
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) => const MaterialApp(home: MyHomePage());
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int num = 1;

  void incrementCounter() {
    setState(() {
      num = Random().nextInt(9);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My App",
      theme: ThemeData(primarySwatch: Colors.deepPurple),
      home: Scaffold(
        appBar: AppBar(title: const Text("My Gallery")),
        backgroundColor: Colors.grey,
        body:
            Center(child: Expanded(child: Image.asset('images/food$num.jpg'))),
        floatingActionButton: FloatingActionButton(
          onPressed: incrementCounter,
          child: const Icon(Icons.shuffle),
        ),
      ),
    );
  }
}
