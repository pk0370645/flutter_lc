import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  static const String _title = 'Menu Card';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: const MyStatelessWidget(),
      ),
    );
  }
}

class CustomListItem extends StatelessWidget {
  const CustomListItem({
    super.key,
    required this.thumbnail,
    required this.title,
    required this.type,
    required this.price,
    required this.ratings,
  });

  final Widget thumbnail;
  final String title;
  final String type;
  final double price;
  final double ratings;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: thumbnail,
          ),
          Expanded(
            flex: 3,
            child: _FoodDescription(
              title: title,
              type: type,
              price: price,
              ratings: ratings,
            ),
          ),
        ],
      ),
    );
  }
}

class _FoodDescription extends StatelessWidget {
  const _FoodDescription({
    required this.title,
    required this.type,
    required this.price,
    required this.ratings,
  });

  final String title;
  final String type;
  final double price;
  final double ratings;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: const TextStyle(
              fontWeight: FontWeight.w900,
              fontSize: 19.0,
            ),
          ),
          const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
          Text(
            type,
            style: const TextStyle(fontSize: 15.0),
          ),
          const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
          Text(
            'Price: ₹$price',
            style: const TextStyle(fontSize: 15.0),
          ),
          const Padding(padding: EdgeInsets.symmetric(vertical: 1.0)),
          Text(
            '$ratings Stars',
            style: const TextStyle(fontSize: 15.0),
          ),
        ],
      ),
    );
  }
}

class MyStatelessWidget extends StatelessWidget {
  const MyStatelessWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(8.0),
      itemExtent: 106.0,
      children: <CustomListItem>[
        CustomListItem(
          type: 'Veg',
          price: 12.99,
          ratings: 4.7,
          thumbnail: Image.asset('images/samosa.jpg'),
          title: 'Samosa',
        ),
        CustomListItem(
          type: 'Veg',
          price: 29.99,
          ratings: 4.6,
          thumbnail: Image.asset('images/fries.jpg'),
          title: 'French Fries',
        ),
        CustomListItem(
          type: 'Veg',
          price: 39.99,
          ratings: 4.9,
          thumbnail: Image.asset('images/chowmein.jpg'),
          title: 'Chowmein',
        ),
        CustomListItem(
          type: 'Veg',
          price: 199.99,
          ratings: 4.9,
          thumbnail: Image.asset('images/pizza.jpg'),
          title: 'Pizza',
        ),
        CustomListItem(
          type: 'Veg',
          price: 29.99,
          ratings: 4.8,
          thumbnail: Image.asset('images/momos.jpg'),
          title: 'Momos',
        ),
        CustomListItem(
          type: 'Non-Veg',
          price: 99.99,
          ratings: 5,
          thumbnail: Image.asset('images/chicken.jpg'),
          title: 'Chicken Karahi',
        ),
        CustomListItem(
          type: 'Veg',
          price: 59.99,
          ratings: 5,
          thumbnail: Image.asset('images/paneer.jpg'),
          title: 'Paneer',
        ),
        CustomListItem(
          type: 'Veg',
          price: 12.99,
          ratings: 4.7,
          thumbnail: Image.asset('images/chapati.jpg'),
          title: 'Chapati',
        ),
        CustomListItem(
          type: 'Veg',
          price: 19.99,
          ratings: 5,
          thumbnail: Image.asset('images/rice.jpg'),
          title: 'Rice',
        ),
        CustomListItem(
          type: 'Veg',
          price: 29.99,
          ratings: 5,
          thumbnail: Image.asset('images/halwa.jpg'),
          title: 'Halwa',
        ),
        CustomListItem(
          type: 'Veg',
          price: 29.99,
          ratings: 4.9,
          thumbnail: Image.asset('images/gulabJamun.jpg'),
          title: 'Gulab Jamun',
        ),
        CustomListItem(
          type: 'Veg',
          price: 32.99,
          ratings: 4.8,
          thumbnail: Image.asset('images/IceCream.jpg'),
          title: 'Ice Cream',
        ),
      ],
    );
  }
}
